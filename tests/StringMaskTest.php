<?php

namespace Tests;


use Ox3a\Filter\Mask\StringMask;

require_once __DIR__ . '/NumberMaskTest.php';

class StringMaskTest extends NumberMaskTest
{

    /**
     * @param array  $data
     * @param string $expected
     * @dataProvider simpleDataProvider
     */
    public function testSimple($data, $expected)
    {
        $number = new StringMask(
            [
                'db'     => $this->getDb(),
                'name'   => 'field1',
                'target' => ['where' => 't.field1'],
            ]
        );

        $conditions = $number->getMask($data);
        $actual     = [];

        foreach ($conditions as $condition) {
            $actual[strtolower($condition->getTarget())] = $condition->getCondition();
        }

        $this->assertEquals($expected, $actual, print_r($actual, true));
    }


    /**
     * @param $data
     * @param $expected
     * @dataProvider multiDataProvider
     */
    public function testMulti($data, $expected)
    {
        $number = new StringMask(
            [
                'db'     => $this->getDb(),
                'name'   => 'field1',
                'target' => ['where' => 't.field1'],
            ]
        );

        $conditions = $number->getMask($data);
        $actual     = [];

        foreach ($conditions as $condition) {
            $actual[strtolower($condition->getTarget())] = $condition->getCondition();
        }

        $this->assertEquals($expected, $actual, print_r($actual, true));
    }


    public function simpleDataProvider()
    {
        return [
            // equals
            [['type' => 'equals', 'filter' => '3'], ["where" => "(t.field1 = '3')"]],
            [['type' => 'equals', 'filter' => 3], ["where" => "(t.field1 = '3')"]],
            // notEqual
            [['type' => 'notEqual', 'filter' => '3'], ["where" => "(t.field1 <> '3')"]],
            [['type' => 'notEqual', 'filter' => 3], ["where" => "(t.field1 <> '3')"]],
            // lessThan
            [['type' => 'startsWith', 'filter' => '3'], ["where" => "(t.field1 LIKE '3%')"]],
            [['type' => 'startsWith', 'filter' => 3], ["where" => "(t.field1 LIKE '3%')"]],
            // lessThanOrEqual
            [['type' => 'endsWith', 'filter' => '3'], ["where" => "(t.field1 LIKE '%3')"]],
            [['type' => 'endsWith', 'filter' => 3], ["where" => "(t.field1 LIKE '%3')"]],
            // greaterThan
            [['type' => 'contains', 'filter' => '3'], ["where" => "(t.field1 LIKE '%3%')"]],
            [['type' => 'contains', 'filter' => 3], ["where" => "(t.field1 LIKE '%3%')"]],
            // greaterThanOrEqual
            [['type' => 'notContains', 'filter' => '3'], ["where" => "(t.field1 NOT LIKE '%3%')"]],
            [['type' => 'notContains', 'filter' => 3], ["where" => "(t.field1 NOT LIKE '%3%')"]],
        ];
    }
}
