<?php


namespace Tests;

require_once __DIR__ . '/NumberMaskTest.php';

use Ox3a\Filter\Mask\DateMask;

class DateMaskTest extends NumberMaskTest
{

    /**
     * @param array  $data
     * @param string $expected
     * @dataProvider simpleDataProvider
     */
    public function testSimple($data, $expected)
    {
        $number = new DateMask(
            [
                'db'     => $this->getDb(),
                'name'   => 'field1',
                'target' => 't.field1',
            ]
        );

        $conditions = $number->getMask($data);
        $actual     = [];

        foreach ($conditions as $condition) {
            $actual[strtolower($condition->getTarget())] = $condition->getCondition();
        }

        $this->assertEquals($expected, $actual, print_r($actual, true));
    }


    /**
     * @param $data
     * @param $expected
     * @dataProvider multiDataProvider
     */
    public function testMulti($data, $expected)
    {
        $number = new DateMask(
            [
                'db'     => $this->getDb(),
                'name'   => 'field1',
                'target' => ['where' => 't.field1'],
            ]
        );

        $conditions = $number->getMask($data);
        $actual     = [];
        foreach ($conditions as $condition) {
            $actual[strtolower($condition->getTarget())] = $condition->getCondition();
        }

        $this->assertEquals($expected, $actual, print_r($actual, true));
    }
}
