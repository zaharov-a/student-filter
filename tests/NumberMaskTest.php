<?php

namespace Tests;

use Ox3a\Filter\Mask\NumberMask;
use Ox3a\Service\ConfigService;
use Ox3a\Service\DbService;
use PHPUnit\Framework\TestCase;

class NumberMaskTest extends TestCase
{
    /**
     * @param array  $data
     * @param string $expected
     * @dataProvider simpleDataProvider
     */
    public function testSimple($data, $expected)
    {
        $number = new NumberMask(
            [
                'db'     => $this->getDb(),
                'name'   => 'field1',
                'target' => ['where' => 't.field1'],
            ]
        );

        $conditions = $number->getMask($data);
        $actual     = [];

        foreach ($conditions as $condition) {
            $actual[strtolower($condition->getTarget())] = $condition->getCondition();
        }

        $this->assertEquals($expected, $actual, print_r($actual, true));
    }


    /**
     * @param $data
     * @param $expected
     * @dataProvider multiDataProvider
     */
    public function testMulti($data, $expected)
    {
        $number = new NumberMask(
            [
                'db'     => $this->getDb(),
                'name'   => 'field1',
                'target' => ['where' => 't.field1'],
            ]
        );

        $conditions = $number->getMask($data);
        $actual     = [];

        foreach ($conditions as $condition) {
            $actual[strtolower($condition->getTarget())] = $condition->getCondition();
        }

        $this->assertEquals($expected, $actual, print_r($actual, true));
    }


    public function simpleDataProvider()
    {
        return [
            // equals
            [['type' => 'equals', 'filter' => '3'], ["where" => "(t.field1 = '3')"]],
            [['type' => 'equals', 'filter' => 3], ["where" => "(t.field1 = '3')"]],
            // notEqual
            [['type' => 'notEqual', 'filter' => '3'], ["where" => "(t.field1 <> '3')"]],
            [['type' => 'notEqual', 'filter' => 3], ["where" => "(t.field1 <> '3')"]],
            // lessThan
            [['type' => 'lessThan', 'filter' => '3'], ["where" => "(t.field1 < '3')"]],
            [['type' => 'lessThan', 'filter' => 3], ["where" => "(t.field1 < '3')"]],
            // lessThanOrEqual
            [['type' => 'lessThanOrEqual', 'filter' => '3'], ["where" => "(t.field1 <= '3')"]],
            [['type' => 'lessThanOrEqual', 'filter' => 3], ["where" => "(t.field1 <= '3')"]],
            // greaterThan
            [['type' => 'greaterThan', 'filter' => '3'], ["where" => "(t.field1 > '3')"]],
            [['type' => 'greaterThan', 'filter' => 3], ["where" => "(t.field1 > '3')"]],
            // greaterThanOrEqual
            [['type' => 'greaterThanOrEqual', 'filter' => '3'], ["where" => "(t.field1 >= '3')"]],
            [['type' => 'greaterThanOrEqual', 'filter' => 3], ["where" => "(t.field1 >= '3')"]],
            // inRange
            [['type' => 'inRange', 'filter' => '3', 'filterTo' => '6'], ["where" => "(t.field1 BETWEEN '3' AND '6')"]],
            [['type' => 'inRange', 'filter' => 3, 'filterTo' => '6'], ["where" => "(t.field1 BETWEEN '3' AND '6')"]],
        ];
    }


    public function multiDataProvider()
    {
        $parts = $this->simpleDataProvider();
        $data  = [];

        for ($i = 0; $i < count($parts) * 3; $i++) {
            $condition1 = $parts[array_rand($parts)];
            $condition2 = $parts[array_rand($parts)];
            $operator   = rand(0, 1) ? 'AND' : 'OR';
            $data[]     = [
                [
                    'condition1' => $condition1[0],
                    'condition2' => $condition2[0],
                    'operator'   => $operator,
                ],
                ["where" => "({$condition1[1]['where']} {$operator} {$condition2[1]['where']})"],
            ];
        }

        return $data;
    }


    public function getDb()
    {
        $config = new ConfigService();
        $config->set(
            'db',
            [
                'driver'   => 'Pdo_Sqlite',
                'database' => __DIR__ . '/../tmp/test.db',
            ]
        );
        return new DbService($config);
    }
}
