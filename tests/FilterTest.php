<?php

namespace Tests;

use Ox3a\Filter\AbstractFilter;
use Ox3a\Filter\Mask\MaskInterface;
use Ox3a\Filter\Mask\NumberMask;
use Ox3a\Service\ConfigService;
use Ox3a\Service\DbService;
use PHPUnit\Framework\TestCase;

class FilterTest extends TestCase
{

    /**
     * @param $maskData
     * @param $expected
     * @dataProvider dataProvider1
     */
    public function test1($maskData, $expected)
    {
        $filter = $this->getFilter();

        $this->assertEquals($expected, $filter->setMaskData($maskData)->buildQuery());
    }


    public function dataProvider1()
    {
        $baseSql = $this->getFilter()->getResultSql();

        return [
            [[], str_replace('{WHERE}', '', $baseSql) . ' LIMIT 0, 50'],
            [
                ['field1' => ['filter' => '3', 'type' => 'equals']],
                str_replace('{WHERE}', " WHERE (t1.field1 = '3')", $baseSql) . ' LIMIT 0, 50',
            ],
            [
                [
                    'field1' => ['filter' => '3', 'type' => 'equals'],
                    'field2' => ['filter' => '3', 'type' => 'equals'],
                ],
                str_replace('{WHERE}', " WHERE (t1.field1 = '3') AND (t1.field2 = '3')", $baseSql) . ' LIMIT 0, 50',
            ],
            [
                [
                    'field1' => ['filter' => '3', 'type' => 'equals'],
                    'field2' => ['filter' => '3', 'type' => 'lessThan'],
                    'field3' => ['filter' => '3', 'type' => 'notEqual'],
                ],
                str_replace(
                    '{WHERE}',
                    " WHERE (t1.field1 = '3') AND (t1.field2 < '3')",
                    $baseSql
                ) . " HAVING (t1.field4 <> '3') LIMIT 0, 50",
            ],
        ];
    }


    public function getDb()
    {
        $config = new ConfigService();
        $config->set(
            'db',
            [
                'driver'   => 'Pdo_Sqlite',
                'database' => __DIR__ . '/../tmp/test.db',
            ]
        );
        return new DbService($config);
    }


    public function getFilter()
    {
        $filter = new TestFilter($this->getDb());

        $filter
            ->addMask(
                [
                    'type'   => NumberMask::class,
                    'name'   => 'field1',
                    'target' => ['where' => 't1.field1'],
                ]
            )
            ->addMask(
                [
                    'type'   => NumberMask::class,
                    'name'   => 'field2',
                    'target' => [MaskInterface::TARGET_WHERE => 't1.field2'],
                ]
            )
            ->addMask(
                [
                    'type'   => NumberMask::class,
                    'name'   => 'field3',
                    'target' => [MaskInterface::TARGET_HAVING => 't1.field4'],
                    'expr'   => 't1.field4',
                ]
            );

        return $filter;
    }
}


class TestFilter extends AbstractFilter
{
    public function getResultSql()
    {
        return '
        SELECT `t1`.*, `t2`.* 
        FROM `table1` AS `t1` 
        JOIN `table2` AS `t2` 
            ON (`t1`.`field1` = `t2`.`field2`) 
            {WHERE}
        GROUP BY t1.field2
        ';
    }


    public function getResultCountSqlQuery()
    {
        return 'select 1';
    }


    public function setMask()
    {
        // TODO: Implement setMask() method.
    }

}
