<?php


namespace Ox3a\Filter\Mask;


use RuntimeException;

class NumberMask extends AbstractMask
{


    protected $_types = [
        'select',
        'equals',
        'notEqual',
        'lessThan',
        'lessThanOrEqual',
        'greaterThan',
        'greaterThanOrEqual',
        'inRange',
    ];


    public function getCondition($data)
    {
        if (!in_array($data['type'], $this->_types)) {
            throw new RuntimeException(sprintf('Неизвестный тип условия: %s', $data['type']));
        }

        $type = $data['type'];

        return $this->$type($data);
    }


    public function select($data)
    {
        return $this->_makeResult('(%s IN (%s))', $this->_dbService->quote($data['filter']));
    }


    public function equals($data)
    {
        return $this->_makeResult('(%s = %s)', $this->_dbService->quote($data['filter']));
    }


    public function notEqual($data)
    {
        return $this->_makeResult('(%s <> %s)', $this->_dbService->quote($data['filter']));
    }


    public function lessThan($data)
    {
        return $this->_makeResult('(%s < %s)', $this->_dbService->quote($data['filter']));
    }


    public function lessThanOrEqual($data)
    {
        return $this->_makeResult('(%s <= %s)', $this->_dbService->quote($data['filter']));
    }


    public function greaterThan($data)
    {
        return $this->_makeResult('(%s > %s)', $this->_dbService->quote($data['filter']));
    }


    public function greaterThanOrEqual($data)
    {
        return $this->_makeResult('(%s >= %s)', $this->_dbService->quote($data['filter']));
    }


    public function inRange($data)
    {
        return $this->_makeResult(
            '(%s BETWEEN %s AND %s)',
            [
                $this->_dbService->quote($data['filter']),
                $this->_dbService->quote($data['filterTo']),
            ]
        );
    }
}
