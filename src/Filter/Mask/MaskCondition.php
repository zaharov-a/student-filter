<?php


namespace Ox3a\Filter\Mask;


class MaskCondition
{
    /**
     * @var string
     */
    protected $_target;

    /**
     * @var string
     */
    protected $_condition;


    /**
     * MaskCondition constructor.
     * @param string $_target
     * @param string $_condition
     */
    public function __construct($_target, $_condition)
    {
        $this->setTarget($_target);
        $this->setCondition($_condition);
    }


    /**
     * @return string
     */
    public function getTarget()
    {
        return $this->_target;
    }


    /**
     * @param string $target
     * @return $this
     */
    public function setTarget($target)
    {
        $this->_target = strtoupper($target);
        return $this;
    }


    /**
     * @return string
     */
    public function getCondition()
    {
        return $this->_condition;
    }


    /**
     * @param string $condition
     * @return $this
     */
    public function setCondition($condition)
    {
        $this->_condition = $condition;
        return $this;
    }


}
