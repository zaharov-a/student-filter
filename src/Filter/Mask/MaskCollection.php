<?php


namespace Ox3a\Filter\Mask;


class MaskCollection
{
    protected $_list = [];


    /**
     * @param MaskInterface $mask
     * @return $this
     */
    public function add(MaskInterface $mask)
    {
        $this->_list[$mask->getName()] = $mask;
        return $this;
    }


    /**
     * @param $name
     * @return MaskInterface|null
     */
    public function get($name)
    {
        if (array_key_exists($name, $this->_list)) {
            return $this->_list[$name];
        }
        return null;
    }


    /**
     * @return MaskInterface[]
     */
    public function getList()
    {
        return $this->_list;
    }
}
