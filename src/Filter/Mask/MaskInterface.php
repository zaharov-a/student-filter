<?php


namespace Ox3a\Filter\Mask;


interface MaskInterface
{
    const TARGET_WHERE = 'where';

    const TARGET_HAVING = 'having';


    /**
     * @param $data
     * @return MaskCondition[]
     */
    public function getMask($data);


    public function getName();


    /**
     * @return MaskInterface
     */
    public function enable();


    /**
     * @return MaskInterface
     */
    public function disable();


    /**
     * @return bool
     */
    public function isEnabled();

}
