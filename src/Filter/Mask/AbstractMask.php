<?php


namespace Ox3a\Filter\Mask;


use Ox3a\Service\DbServiceInterface;

abstract class AbstractMask implements MaskInterface
{

    /**
     * @var DbServiceInterface
     */
    protected $_dbService;

    /**
     * @example
     * [target=>expr][]
     * [
     *  'where' => 't.field',
     *  'where2' => 't2.field',
     * ]
     * @var string[]
     */
    protected $_target = [];

    protected $_name = '';

    /**
     * @var string
     */
    protected $_caption;

    /**
     * @var string|array
     */
    protected $_types;

    /**
     * @var string
     */
    protected $_filterUse;

    /**
     * @var
     */
    protected $_filterType;


    /**
     * @var bool
     */
    protected $_maskUse = true;

    /**
     * @var bool
     */
    protected $_enabled = true;


    /**
     * @param array $data
     * @return MaskCondition[]
     */
    abstract public function getCondition($data);


    public function __construct($params = [])
    {
        if (isset($params['db'])) {
            $params['dbService'] = $params['db'];
        }

        foreach ($params as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }


    /**
     * @return DbServiceInterface
     */
    public function getDbService()
    {
        return $this->_dbService;
    }


    /**
     * @param DbServiceInterface $db
     * @return $this
     */
    public function setDbService($db)
    {
        $this->_dbService = $db;
        return $this;
    }


    /**
     * @return string[]
     */
    public function getTarget()
    {
        return $this->_target;
    }


    /**
     * @param string[]|string $target
     * @return $this
     */
    public function setTarget($target)
    {
        if (!is_array($target)) {
            $target = [MaskInterface::TARGET_WHERE => $target];
        }
        $this->_target = $target;
        return $this;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }


    /**
     * @param mixed|string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->_name = $name;
        if (empty($this->getTarget())) {
            $this->setTarget($name);
        }
        return $this;
    }


    /**
     * @return string
     */
    public function getCaption()
    {
        return $this->_caption;
    }


    /**
     * @param string $caption
     * @return $this
     */
    public function setCaption($caption)
    {
        $this->_caption = $caption;
        return $this;
    }


    /**
     * @return $this|MaskInterface
     */
    public function enable()
    {
        $this->_enabled = true;
        return $this;
    }


    /**
     * @return $this|MaskInterface
     */
    public function disable()
    {
        $this->_enabled = false;
        return $this;
    }


    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->_enabled;
    }


    /**
     * @param $data
     * @return MaskCondition[]
     */
    public function getMask($data)
    {
        // если есть оператор, то это составное условие
        if (isset($data['operator'])) {
            $part1 = $this->getCondition($data['condition1']);
            $part2 = $this->getCondition($data['condition2']);

            $operator = strtolower($data['operator']) == 'or' ? 'OR' : 'AND';

            $result = [];

            foreach ($part1 as $index => $condition1) {
                $condition2 = $part2[$index];
                $result[]   = new MaskCondition(
                    $condition1->getTarget(),
                    sprintf("(%s %s %s)", $condition1->getCondition(), $operator, $condition2->getCondition())
                );
            }

            return $result;
        }

        return $this->getCondition($data);
    }


    /**
     * @param string          $template
     * @param string|string[] $value
     * @return MaskCondition[]
     */
    protected function _makeResult($template, $value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        return array_map(
            function ($target, $expr) use ($template, $value) {
                $sprintfParams = array_merge([$template, $expr], is_array($value) ? $value : [$value]);

                return new MaskCondition($target, call_user_func_array('sprintf', $sprintfParams));
            },
            array_keys($this->_target),
            $this->_target
        );
    }
}
