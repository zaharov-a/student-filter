<?php


namespace Ox3a\Filter\Mask;

use RuntimeException;

class StringMask extends AbstractMask
{

    protected $_types = [
        'select',
        'equals',
        'notEqual',
        'startsWith',
        'endsWith',
        'contains',
        'notContains',
    ];


    public function getCondition($data)
    {
        if (!in_array($data['type'], $this->_types)) {
            throw new RuntimeException(sprintf('Неизвестный тип условия: %s', $data['type']));
        }

        $type = $data['type'];

        return $this->$type($data);
    }


    public function select($data)
    {
        return $this->_makeResult('(%s IN (%s))', $this->_dbService->quote($data['filter']));
    }


    public function equals($data)
    {
        return $this->_makeResult('(%s = %s)', $this->_dbService->quote($data['filter']));
    }


    public function notEqual($data)
    {
        return $this->_makeResult('(%s <> %s)', $this->_dbService->quote($data['filter']));
    }


    public function startsWith($data)
    {
        return $this->_makeResult('(%s LIKE %s)', $this->_dbService->quote($data['filter'] . '%'));
    }


    public function notStartsWith($data)
    {
        return $this->_makeResult('(%s NOT LIKE %s)', $this->_dbService->quote($data['filter'] . '%'));
    }


    public function endsWith($data)
    {
        return $this->_makeResult('(%s LIKE %s)', $this->_dbService->quote('%' . $data['filter']));
    }


    public function notEndsWith($data)
    {
        return $this->_makeResult('(%s NOT LIKE %s)', $this->_dbService->quote('%' . $data['filter']));
    }


    public function contains($data)
    {
        return $this->_makeResult('(%s LIKE %s)', $this->_dbService->quote('%' . $data['filter'] . '%'));
    }


    public function notContains($data)
    {
        return $this->_makeResult('(%s NOT LIKE %s)', $this->_dbService->quote('%' . $data['filter'] . '%'));
    }


}
