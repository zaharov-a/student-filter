<?php


namespace Ox3a\Filter;


abstract class AbstractPageFilter extends AbstractFilter
{

    protected $_page = 1;

    protected $_pageCount;

    protected $_rowCount = 10;


    /**
     * @return int
     */
    public function getPage()
    {
        return $this->_page;
    }


    /**
     * @param int $page
     * @return $this
     */
    public function setPage($page)
    {
        $this->_page = max(1, $page);
        $this->_page = min($this->_page, $this->getPageCount());
        return $this
            ->setStartRow($this->_rowCount * ($this->_page - 1))
            ->setEndRow($this->_rowCount * $this->_page);
    }


    /**
     * @return mixed
     */
    public function getPageCount()
    {
        if (!$this->_pageCount) {
            $rowCount         = $this->getTotalCount();
            $this->_pageCount = max(ceil($rowCount / $this->_rowCount), 1);
        }
        return $this->_pageCount;
    }


    /**
     * @param mixed $pageCount
     * @return AbstractPageFilter
     */
    public function setPageCount($pageCount)
    {
        $this->_pageCount = $pageCount;
        return $this;
    }


    /**
     * @return int
     */
    public function getRowCount()
    {
        return $this->_rowCount;
    }


    /**
     * @param int $rowCount
     * @return AbstractPageFilter
     */
    public function setRowCount($rowCount)
    {
        $this->_rowCount = $rowCount;
        return $this;
    }


    public function getResultCountSqlQuery()
    {
        $old = $this->getEndRow();
        $this->setEndRow($this->getStartRow());
        $sql = sprintf("SELECT COUNT(*) FROM (%s) AS `t`", $this->buildQuery());
        $this->setEndRow($old);

        return $sql;
    }


}
