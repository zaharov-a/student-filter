<?php


namespace Ox3a\Filter;


use Ox3a\Filter\Mask\MaskCollection;
use Ox3a\Filter\Mask\MaskInterface;
use Ox3a\Service\DbServiceInterface;

abstract class AbstractFilter
{
    protected $_order = [];

    protected $_startRow = 0;

    protected $_endRow = 50;

    protected $_where = [];

    protected $_having = [];

    protected $_options = [];

    /**
     * @var DbServiceInterface
     */
    protected $_dbService;

    protected $_maskData;

    /**
     * @var MaskCollection
     */
    protected $_masks;


    abstract public function getResultSql();


    abstract public function getResultCountSqlQuery();


    /**
     * AbstractFilter constructor.
     * @param DbServiceInterface $dbService
     */
    public function __construct(DbServiceInterface $dbService)
    {
        $this->_dbService = $dbService;
        $this->_masks     = new MaskCollection();
    }


    /**
     * @param array $options
     * @return AbstractFilter
     */
    public function setOptions($options)
    {
        $this->_options = $options;
        return $this;
    }


    /**
     * @param mixed|null $options
     * @return $this
     */
    public function init($options = null)
    {
        return $this;
    }


    /**
     * @return int
     */
    public function getStartRow()
    {
        return $this->_startRow;
    }


    /**
     * @param int $startRow
     * @return $this
     */
    public function setStartRow($startRow)
    {
        $this->_startRow = $startRow;
        return $this;
    }


    /**
     * @return int
     */
    public function getEndRow()
    {
        return $this->_endRow;
    }


    /**
     * @param int $endRow
     * @return $this
     */
    public function setEndRow($endRow)
    {
        $this->_endRow = $endRow;
        return $this;
    }


    /**
     * @param array|string $order
     * @return $this
     */
    public function setOrder($order)
    {
        if (is_string($order)) {
            $order = [$order];
        }
        $this->_order = $order;
        return $this;
    }


    /**
     * @return MaskCollection
     */
    public function getMasks()
    {
        return $this->_masks;
    }


    /**
     * @param array $data
     * @return $this
     */
    public function setMaskData($data)
    {
        $this->_maskData = $this->prepareMaskData($data);

        return $this;
    }


    /**
     * @param array|MaskInterface $maskOrData
     * @return $this
     */
    public function addMask($maskOrData)
    {
        if ($maskOrData instanceof MaskInterface) {
            $mask = $maskOrData;
        } else {
            $maskOrData['db'] = $this->_dbService;

            $class = $maskOrData['type'];
            $mask  = new $class($maskOrData);
        }
        $this->getMasks()->add($mask);
        return $this;
    }


    public function getResult()
    {
        return $this->getRows();
    }


    public function getRows()
    {
        $sql = $this->buildQuery();

        if (defined('FILTER_SQL_FORMATTER_DEBUG')) {
            exit("\r\n<pre>\r\n" . __FILE__ . ':' . __LINE__ . "\r\n" . print_r([$sql], true) . "\r\n</pre>\r\n");
        }

        return $this->_dbService->fetchAll($sql);
    }


    public function applyMask()
    {
        $this->clearHaving();
        $this->clearWhere();

        $data = $this->_maskData ?: [];
        foreach ($this->getMasks()->getList() as $mask) {
            if (!$mask->isEnabled()) {
                continue;
            }
            $fieldName = $mask->getName();
            if (array_key_exists($fieldName, $data)) {
                $value    = $data[$fieldName];
                $maskList = $mask->getMask($value);
                foreach ($maskList as $condition) {
                    $this->appendCondition($condition);
                }
            }
        }
    }


    public function buildQuery()
    {
        $this->applyMask();

        $query  = $this->getResultSql();
        $where  = $this->getWhereClause();
        $having = $this->getHavingClause();
        $limit  = $this->getLimitClause();
        $order  = $this->getOrderClause();

        foreach ($where as $target => $condition) {
            $query = str_replace("{{$target}}", $condition, $query);
        }

        return $query
            . $having
            . $order
            . $limit;
    }


    public function getWhereClause()
    {
        return array_map(
            function ($where) {
                return $where ? (' WHERE ' . implode(' AND ', $where)) : '';
            },
            $this->_where
        );
    }


    public function getHavingClause()
    {
        if (!$this->_having) {
            return '';
        }

        return ' HAVING ' . implode(' AND ', $this->_having);
    }


    public function getLimitClause()
    {
        $startRow = (int)$this->_startRow;
        $limit    = $this->_endRow - $this->_startRow;

        if ($limit < 1) {
            return '';
        }

        return sprintf(' LIMIT %s, %s', $startRow, $limit);
    }


    public function getOrderClause()
    {
        if (!$this->_order) {
            return '';
        }

        $order = [];

        foreach ($this->_order as $key => $value) {
            if (is_numeric($key)) {
                $order[] = $this->_dbService->quoteIdentifier($value);
            } else {
                $order[] = $this->_dbService->quoteIdentifier($key) . ' ' . ($value == 'desc' ? 'DESC' : 'ASC');
            }
        }


        return ' ORDER BY ' . implode(', ', $order);
    }


    public function getTotalCount()
    {
        return $this->_dbService->fetchOne($this->getResultCountSqlQuery());
    }


    private function appendHaving(Mask\MaskCondition $condition)
    {
        $this->_having[] = $condition->getCondition();
    }


    private function appendWhere(Mask\MaskCondition $condition)
    {
        if (!isset($this->_where[$condition->getTarget()])) {
            $this->_where[$condition->getTarget()] = [];
        }
        $this->_where[$condition->getTarget()][] = $condition->getCondition();
    }


    protected function clearHaving()
    {
        $this->_having = [];
    }


    protected function clearWhere()
    {
        $list = array_unique(
            array_reduce(
                array_map(
                    function ($mask) {
                        return array_keys($mask->getTarget());
                    },
                    $this->getMasks()->getList()
                ),
                'array_merge',
                []
            )
        );


        $this->_where = array_fill_keys(array_map('strtoupper', $list), []);
    }


    protected function appendCondition(Mask\MaskCondition $condition)
    {
        if (strtolower($condition->getTarget()) == MaskInterface::TARGET_HAVING) {
            $this->appendHaving($condition);
        } else {
            $this->appendWhere($condition);
        }
    }


    /**
     * @param array $data
     * @return array
     */
    protected function prepareMaskData($data)
    {
        return $data;
    }
}
